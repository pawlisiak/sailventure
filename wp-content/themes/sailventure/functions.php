<?php

// Return the templates directory path
add_filter('template', function ($stylesheet) {
	return dirname($stylesheet);
});

// Include template parts from 'templates' directory for better files hierarchy
add_action('after_switch_theme', function () {
	$stylesheet = get_option('template');
	if (basename($stylesheet) !== 'templates')
		update_option('template', $stylesheet . '/templates');
});

// Include code library
$includes = array(
	'lib/setup.php',				// Theme setup
	'lib/customizer.php',		// Theme customizer
	'lib/filters.php',			// WP filters
	'lib/helpers.php',			// Helping functions
	'lib/shortcodes.php',		// Shortcodes
	'lib/post-types.php'		// Custom post types
);

array_walk($includes, function ($file) {
	if (!locate_template($file, true, true))
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sailventure'), $file), E_USER_ERROR);
});
