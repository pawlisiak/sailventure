<?php get_template_part('parts/header'); ?>

	<?php if (have_posts()) : the_post(); ?>

		<?php get_template_part('parts/content', 'dialog-cruise'); ?>

	<?php endif; ?>

<?php get_template_part('parts/footer');
