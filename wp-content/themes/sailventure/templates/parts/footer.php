		</main>

		<!-- MAIN: End -->

		<!-- FOOTER: Start -->

		<footer class="c-siteFooter [ row fluid ]">
			<div class="c-siteFooter__hr [ sm-12 md-10 md-off-1 column ]">
				<hr>
			</div>

			<div class="c-siteFooter__nav [ sm-12 md-5 md-off-1 column ]"></div>
			<div class="c-siteFooter__copy [ sm-12 md-5 column align-right ]">
				<?php _e('Designed & developed with <em>&#9829;</em> by', 'sailventure'); ?> inksze.
			</div>
		</footer>

		<!-- FOOTER: End -->

		<?php wp_footer(); ?>

	</body>
</html>
