<div class="c-cruises [ row fluid ]">
	<article class="[ xs-12 lg-4 xl-3 xl-off-1 column ]">
		<?php the_content(''); ?>
	</article>

	<div class="c-cruises__list [ xs-12 lg-7 lg-off-1 xl-6 column ] [ row fluid ]">

		<?php

			$cruises = get_posts(
				array(
					'post_type'			=> 'cruise',
					'posts_per_page'	=> 999,
					'orderby'			=> 'menu_order',
					'order'				=> 'ASC'
				)
			);

			foreach ($cruises as $post) {
				setup_postdata($post);
				get_template_part('parts/content', 'single-cruise');
			}

			wp_reset_postdata();

		?>

	</div>

	<div class="c-cruises__dialogs">

		<?php foreach ($cruises as $post) : setup_postdata($post); ?>

			<div class="c-cruiseDialog" id="cruise-<?php the_ID(); ?>">
				<div class="c-cruiseDialog__header">
					<button class="m-icon" data-close="#cruise-<?php the_ID(); ?>">
						<svg class="icon32"><use xlink:href="#icon32-close"></use></svg>
					</button>
				</div>

				<main class="c-cruiseDialog__main [ row fluid ]">

					<?php get_template_part('parts/content', 'dialog-cruise'); ?>

				</main>
			</div>

		<?php endforeach; ?>
		<?php wp_reset_postdata(); ?>

	</div>
</div>
