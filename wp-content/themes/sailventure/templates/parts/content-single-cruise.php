<a href="<?php the_permalink($post->ID); ?>" class="m-cruiseItem [ xs-12 sm-6 md-4 lg-6 xl-4 column ]" data-dialog="#cruise-<?php the_ID(); ?>">
	<div class="m-cruiseItem__thumb" <?php echo (has_post_thumbnail()) ? 'style="background-image: url(' . get_post_image_url($post->ID, 'cruise-thumb') . ');"' : null; ?>></div>
	<h2 class="m-cruiseItem__title"><?php the_title(); ?></h2>
	<h4 class="m-cruiseItem__date"><?php echo date('j.m', strtotime(get_field('date_from'))); ?> - <?php echo date('j.m', strtotime(get_field('date_to'))); ?></h4>
</a>
