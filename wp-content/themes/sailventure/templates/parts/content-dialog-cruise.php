<article class="c-cruiseDialog__content [ xs-12 lg-5 xl-5 xl-off-1 column ]">
	<h1><?php the_title(); ?></h1>
	<h2><?php echo date('j.m', strtotime(get_field('date_from'))); ?> - <?php echo date('j.m', strtotime(get_field('date_to'))); ?></h2>
	<?php the_content(); ?>

	<?php if (!empty(get_field('price'))) : ?>

		<div class="c-cruiseDialog__form [ sm-12 md-10 md-off-1 column ]">
			<span class="m-cruisePrice">
				<small><?php _e('Price', 'sailventure'); ?>:</small>
				<?php the_field('price'); ?>
			</span>

			<button class="btn" data-collapse="#form-<?php the_ID(); ?>"><?php _e('Purchase', 'sailventure'); ?></button>

			<div id="form-<?php the_ID(); ?>" class="m-cruisePurchase">
				<?php echo apply_filters('the_content', get_theme_mod('cruise_form')); ?>
			</div>
		</div>

	<?php endif; ?>

</article>

<aside class="c-cruiseDialog__gallery [ xs-12 lg-6 lg-off-1 xl-5 column ]">
	<?php $location = get_field('map'); ?>

	<?php if (!empty($location)) : ?>
		<div id="map-canvas" class="m-mapCanvas" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
	<?php endif; ?>

	<div class="m-gallery">
		<?php the_field('gallery'); ?>
	</div>
</aside>
