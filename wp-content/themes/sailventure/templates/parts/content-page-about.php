<article class="c-aboutUs [ row fluid ]">
	<div class="c-aboutUs__excerpt [ xs-12 lg-6 xl-4 xl-off-2 column ]">
		<hr>
		<?php global $more; $more = 0; ?>
		<?php the_content(''); ?>
	</div>

	<div class="c-aboutUs__more [ xs-12 lg-6 xl-4 column ]">
		<?php $more = 1; ?>
		<?php the_content('', true); ?>
	</div>

	<?php if (has_post_thumbnail()) : ?>

		<div class="c-aboutUs__background [ sm-12 column ]" style="background-image: url(<?php echo get_post_image_url(get_the_ID(), 'full'); ?>);"></div>

	<?php endif; ?>

</article>
