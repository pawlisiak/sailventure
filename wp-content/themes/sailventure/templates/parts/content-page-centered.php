<div class="c-centered [ row fluid ]">
	<article class="[ xs-12 sm-10 sm-off-1 lg-8 lg-off-2 xl-6 xl-off-3 column align-center ]">
		<hr>
		<?php the_content(); ?>
	</article>
</div>
