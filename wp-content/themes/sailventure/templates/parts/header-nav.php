<header class="c-siteHeader [ row fluid ]">
	<div class="c-siteHeader__background"></div>
	<button class="c-siteHeader__navToggle" data-menu=".c-siteHeader"><svg class="icon32"><use xlink:href="#icon32-nav"></use></svg></button>

	<?php

		wp_nav_menu(
			array(
				'location'		=> 'primary',
				'container'		=> false,
				'menu_class'	=> 'c-siteHeader__nav [ column ] [ row fluid align-center ]'
			)
		);

	?>

</header>
