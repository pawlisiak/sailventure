<article class="c-contact [ row fluid ]">
	<div class="c-contact__form [ xs-12 md-7 lg-6 xl-5 xl-off-1 column ]">
		<?php global $more; $more = 0; ?>
		<?php the_content(''); ?>
	</div>

	<div class="c-contact__info [ xs-12 md-4 md-off-1 column ]">
		<?php $more = 1; ?>
		<?php the_content('', true); ?>
	</div>
</article>
