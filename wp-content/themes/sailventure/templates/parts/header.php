<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php wp_title(); ?></title>

		<script>
			var
				siteURL			= '<?php bloginfo('url'); ?>',
				themeURL		= '<?php bloginfo('template_url'); ?>';
		</script>

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>

		<!-- HEADER: Start -->

		<?php if (!is_front_page()) get_template_part('parts/header', 'nav'); ?>

		<!-- HEADER: End -->

		<!-- MAIN: Start -->

		<main class="c-siteBody [ row fluid ]">
