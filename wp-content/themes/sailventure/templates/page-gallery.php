<?php /* Template Name: Gallery */ ?>

<?php get_template_part('parts/header'); ?>

	<?php if (have_posts()) : the_post(); ?>

		<?php get_template_part('parts/content-page', get_page_template_name()); ?>

	<?php endif; ?>

<?php get_template_part('parts/footer');
