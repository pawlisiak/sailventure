<?php /* Template Name: Home */ ?>

<?php get_template_part('parts/header'); ?>

	<div class="c-homePage" <?php if (get_theme_mod('hero_image')) : ?>style="background-image: url(<?php echo get_theme_mod('hero_image'); ?>);"<?php endif; ?>>

		<?php if (have_posts()) : the_post(); ?>

			<div class="c-homePage__heroBanner [ row fluid ]">
				<svg class="m-siteLogo"><use xlink:href="#logo-full"></use></svg>

				<div class="m-content [ xs-12 md-10 lg-8 xl-6 column ] [ align-center ]">
					<?php the_content(); ?>
				</div>

				<?php get_template_part('parts/header', 'nav'); ?>
			</div>

		<?php endif; ?>

		<?php

			$subitems = get_pages(
				array(
					'sort_column'		=> 'menu_order',
					'sort_order'		=> 'ASC',
					'parent'				=> get_the_ID()
				)
			);

			foreach ($subitems as $post) {
				setup_postdata($post);

				echo '<section class="c-homePage__section [ row fluid ]" id="' . $post->post_name . '">';

					get_template_part('parts/content-page', get_page_template_name());

				echo '</section>';
			}

			wp_reset_postdata();

		?>

	</div>

<?php get_template_part('parts/footer');
