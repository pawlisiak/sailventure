<?php get_template_part('parts/header'); ?>

	<div class="[ sm-12 column ]">
		<?php the_content(); ?>
	</div>

<?php get_template_part('parts/footer');
