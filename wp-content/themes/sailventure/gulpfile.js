'use strict';

var

// Include gulp
	gulp		= require('gulp'),

// Include plugins
	rename			= require('gulp-rename'),
	uglify			= require('gulp-uglify'),
	sass			= require('gulp-sass'),
	concat			= require('gulp-concat'),
	autoprefixer 	= require('gulp-autoprefixer'),
	sourcemaps 		= require('gulp-sourcemaps'),
	watch			= require('gulp-watch'),
	browserSync 	= require('browser-sync').create(),
	gutil			= require('gulp-util'),
	notify 			= require('gulp-notify'),
	zip				= require('gulp-zip'),

// Include configuration
	config			= require('./gulpconfig.js')();

var reportError = function (error) {

	var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

	if (config.errorNotify == true) {
		notify({
			title: 'Task Failed [' + error.plugin + ']',
			message: lineNumber + 'See console.'
		}).write(error);
	}

	var report = '';
	var chalk = gutil.colors.white.bgRed;

	report += chalk('TASK:') + ' [' + error.plugin + ']\n';
	report += chalk('PROB:') + ' ' + error.message + '\n';
	if (error.lineNumber) { report += chalk('LINE:') + ' ' + error.lineNumber + '\n'; }
	if (error.fileName)   { report += chalk('FILE:') + ' ' + error.fileName + '\n'; }

	console.error(report);

	// Inspect the error object
	//console.log(error);

	this.emit('end');
};

gulp.task('styles', function () {
	return gulp.src([
			config.assetsPath + '/styles/*.scss'
		])

		// Save uncompressed
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', reportError))
		.pipe(autoprefixer({browsers: config.compatibility})).on('error', reportError)
		.pipe(sourcemaps.write(undefined, {sourceRoot: null}))
		.pipe(gulp.dest(config.distPath + '/styles/'))
		.pipe(browserSync.stream())

		// Save compressed
		.pipe(rename({suffix: ".min"}))
		.pipe(sass({outputStyle: 'compressed'}).on('error', reportError))
		.pipe(gulp.dest(config.distPath + '/styles/'))
		.pipe(browserSync.stream());
});

gulp.task('scripts', function () {
	return gulp.src(config.scriptsArray)

		// Save uncompressed
		.pipe(sourcemaps.init())
		.pipe(concat({path: 'scripts.js'}).on('error', reportError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.distPath + '/scripts/'))
		.pipe(browserSync.stream())

		// Save compressed
		.pipe(uglify().on('error', reportError))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(config.distPath + '/scripts/'))
		.pipe(browserSync.stream());
});

gulp.task('images', function () {
	return gulp.src([
		config.assetsPath + '/images/*.*'
	])
		.pipe(gulp.dest(config.distPath + '/images/'));
})

gulp.task('watch', ['styles', 'scripts'], function () {
	gulp.watch([config.assetsPath + '**/*.scss'], ['styles']);
	gulp.watch([config.assetsPath + '**/*.js'], ['scripts']);
});

gulp.task('serve', ['watch'], function (callback) {
	browserSync.init({
		proxy: config.domain,
		online: false
	});

	gulp.watch(['**/*.php']).on('change', browserSync.reload);
});

gulp.task('zip', ['default'], () => {
	return gulp.src(config.zipInclude)
		.pipe(zip(config.zipName + '.zip'))
		.pipe(gulp.dest('.'));
});

gulp.task('default', ['styles', 'scripts']);
