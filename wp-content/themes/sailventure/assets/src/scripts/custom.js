
(function ($) {

	'use strict';

	var
		$body		= $('body'),
		$window		= $(window),

		debug		= false;

	$(function () {
		$body.addClass('is-scripted');

		// Sticky header
		$('body.home .c-siteHeader').sticky({
			debug			: debug,
			offsetTop		: $('body.home .c-siteHeader').offset().top - 10
		});

		// Load SVG in footer
		$('body')
			.svgIconSetLoader({
				debug			: debug,
				iconsFileUrl	: themeURL + '/assets/dist/images/logo-set.svg',
				iconsVersion	: Math.random(),
			})
			.svgIconSetLoader({
				debug			: debug,
				iconsFileUrl	: themeURL + '/assets/dist/images/icons-set.svg',
				iconsVersion	: Math.random(),
			});

		// Parallaxed background in header
		$('.c-homePage').parallaxBg({
			debug			: debug,
			leftOffset		: 50,
			leftUnit		: '%',
			speed			: .3
		});

		$('[data-dialog]').on('click', function (e) {
			var
				self = $(this),
				targetID = $(this).data('dialog');

			if ($(targetID).length) {
				e.preventDefault();

				var
					$dialog = $(targetID),
					bodyWidth = $body.outerWidth();

				if ($dialog.find('.m-mapCanvas').length) {
					var
						canvas = $dialog.find('.m-mapCanvas'),
						latitude = canvas.data('lat'),
						longtitude = canvas.data('lng');

					setTimeout(function () {
						initMap(canvas, latitude, longtitude);
					}, 400);
				}

				$dialog.addClass('is-opened').parent().addClass('is-opened');
				$body.addClass('no-scroll').css('margin-right', $body.outerWidth() - bodyWidth);

				$dialog.parent().on('click', function (e) {
					if(e.currentTarget == e.target) {
						e.preventDefault();

						if ($dialog.find('.m-mapCanvas').length) {
							$dialog.find('.m-mapCanvas').html('');
						}

						$dialog.removeClass('is-opened').parent().removeClass('is-opened');

						if (!$('.is-opened').length)
							$body.removeClass('no-scroll').css('margin-right', '');
					}
				});
			}
		});

		$('[data-close]').on('click', function (e) {
			var closeTarget = $(this).data('close');

			if ($(closeTarget).length) {
				e.preventDefault();

				if ($(closeTarget).find('.m-mapCanvas').length) {
					$(closeTarget).find('.m-mapCanvas').html('');
				}

				$(closeTarget).removeClass('is-opened').parent().removeClass('is-opened');

				if (!$('.is-opened').length)
					$body.removeClass('no-scroll').css('margin-right', '');
			}
		});

		$('[data-visibility-key]').each(function () {
			if ($(this).data('visibility-value')) {
				var self = $(this);
				var key = $(self.data('visibility-key'));

				if (key.val() == self.data('visibility-value'))
					self.show();
				else
					self.hide();

				$(self.data('visibility-key')).on('change', function () {
					if ($(this).val() == self.data('visibility-value'))
						self.show();
					else
						self.hide();
				});
			}
		});

		$('a[href*="#"]').on('click', function (e) {
			var target = $(this).attr('href').split('#')[1];

			if ($('#' + target).length) {
				e.preventDefault();

				$('html, body').stop().animate({
					'scrollTop': $('#' + target).offset().top
				}, 600);

				history.replaceState({
					scrollTop: target
				}, target, '#' + target);
			}
		});

		$('[data-collapse]').each(function () {
			var $target = $($(this).data('collapse'));

			if ($target.length) {
				$target.hide();

				$(this).on('click', function (e) {
					e.preventDefault();
					$target.stop().slideToggle().classTobble('is-expanded');
				});
			}
		});

		$('[data-menu]').on('click', function (e) {
			var $target = $($(this).data('menu'));

			if ($target.length) {
				e.preventDefault();
				$target.toggleClass('is-expanded');
			}
		});
	});

})(jQuery);
