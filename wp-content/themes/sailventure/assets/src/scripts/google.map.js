function initMap(canvas, latitude, longtitude) {
	var
		mapCenter = {
			lat: latitude,
			lng: longtitude
		},
		mapZoom = 9,
		mapStyles = [
			{
				'featureType': 'administrative.country',
				'elementType': 'geometry.stroke',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'administrative.land_parcel',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'administrative.neighborhood',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'poi',
				'elementType': 'labels.text',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'road',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'road',
				'elementType': 'labels',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			},
			{
				'featureType': 'water',
				'elementType': 'labels.text',
				'stylers': [
					{
						'visibility': 'off'
					}
				]
			}
		];

	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: mapZoom,
		center: mapCenter,
		disableDefaultUI: true,
		styles: mapStyles
	});
}
