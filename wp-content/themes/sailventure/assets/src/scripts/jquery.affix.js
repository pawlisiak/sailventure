(function ($) {

	'use strict';

	// Default options
	var defaults = {
			debug				: 0,
			offsetTop			: 0,
			offsetBottom		: false,
			targetElem			: false,
			stickyClass			: 'is-affixed',
			stickyCSS			: false,
			unstickyClass		: 'is-unaffixed',
			unstickyCSS			: false
		},

		lastPosition;

	// Throttle function prevent browser from calling function to often on scroll event
	function throttle (callback, interval) {
		var time = Date.now();

		return function () {
			if ((time + interval - Date.now()) < 0) {
				callback();
				time = Date.now();
			}
		};
	}

	$.fn.sticky = function (options) {

		var
			config = $.extend({}, defaults, options),

			_self = this,

			$window = $(window),
			scrollPosition;

		function checkPosition () {
			scrollPosition = $window.scrollTop();

			if (scrollPosition > config.offsetTop) {
				_self.addClass(config.stickyClass);

				if (config.stickyCSS)
					_self.css(config.stickyCSS);

				if (config.debug);
					console.log('Sticky: Object sticked');
			}
			else {
				_self.removeClass(config.stickyClass);

				if (config.stickyCSS)
					_self.attr('style', '');

				if (config.debug);
					console.log('Sticky: Object unsticked');
			}

			if (config.offsetBottom) {
				if (scrollPosition > config.offsetBottom) {
					_self.addClass(config.unstickyClass);

					if (config.unstickyCSS)
						_self.css(config.unstickyCSS);
				}
				else {
					_self.removeClass(config.unstickyClass);

					if (config.unstickyCSS)
						_self.attr('style', '');
				}
			}

			lastPosition = scrollPosition;
		}

		$(function () {
			if (config.debug);
				console.info('Sticky: Plugin loaded');

			checkPosition();

			$window.on('scroll.sticky', throttle(checkPosition, 40));
		});
	};

})(jQuery);
