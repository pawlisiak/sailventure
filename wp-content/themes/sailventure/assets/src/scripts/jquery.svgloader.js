
/*	================================================================================
 *
 *	JQ: SVG ICONSET LOADER
 *
 *	Author		: Bartosz Perończyk (peronczyk.com)
 *	Inspired by	: http://osvaldas.info/caching-svg-sprite-in-localstorage
 *
 *	--------------------------------------------------------------------------------
 *	DESCRIPTION:
 *
 *	Loads to specific element in dom set of SVG icons and store it for
 *	caching purpose in Local Storage.
 *
 *	--------------------------------------------------------------------------------
 *	INSTALATION:
 *
 *	$('body').svgIconSetLoader({
 *		iconsFileUrl: 'path/to/icons.svg',
 *		iconsVersion: 1,
 *		debug: 1
 *	});
 *
 *	If you will not set iconVersion icons set wouldn't becached at all
 *
 *	================================================================================
 */


(function($) {

	'use strict';


	/*	----------------------------------------------------------------------------
	 *	PLUGIN DEFAULT CONFIGURATION
	 */

	var defaults =
		{
			debug			: 0,
			iconsFileUrl	: null,
			iconsVersion	: false,
		},
		data,
		isLocalStorage = 'localStorage' in window && window['localStorage'] !== null;


	/*	----------------------------------------------------------------------------
	 *	SET UP JQUERY PLUGIN
	 */

	$.fn.svgIconSetLoader = function(options) {

		// Setup configuration
		var config	= $.extend({}, defaults, options);

		// Definitions
		var that = this;

		if (config.debug) console.info('Plugin loaded: svgIconSetLoader');

		if (!config.iconsFileUrl) {
			if (config.debug) console.error('svgIconSetLoader: path to icons file not set');
			return that;
		}


		// If SVG icons found in Local Storage and their version equals requested version
		if (config.iconsVersion && isLocalStorage && localStorage.getItem('svgIconSetVer') === config.iconsVersion) {
			data = localStorage.getItem('svgIconSet');
			if (data) {
				that.append(data);
				if (config.debug) console.info('svgIconSetLoader: Icons loaded from Local Storage (ver: ' + config.iconsVersion + ')');
				return that;
			}
		}

		// If there was no data in Local Storage
		// or data has wrong version load data asynchronously
		that.ajaxPromise = $.ajax({
			url			: config.iconsFileUrl,
			cache		: false,
			dataType	: 'html',

			success: function(data) {
				if (data) {
					that.append(data);
					if (config.debug) console.info('svgIconSetLoader: Icons loaded asynchronously from file');

					if (isLocalStorage && config.iconsVersion) {
						localStorage.setItem('svgIconSet', data);
						localStorage.setItem('svgIconSetVer', config.iconsVersion);
						if (config.debug) console.info('svgIconSetLoader: Icons saved to Local Storage (ver: ' + config.iconsVersion + ')');
					}
				}
				else if (config.debug) console.error('svgIconSetLoader: No data received from icons file. Probably file is empty or damaged.');
			},

			error: function(jqXHR, textStatus, errorThrown) {
				if (config.debug) console.error('svgIconSetLoader: Error "' + errorThrown + '" occured while accessing icons file "' + config.iconsFileUrl + '"');
			}
		});

		return that;
	};

})(jQuery);
