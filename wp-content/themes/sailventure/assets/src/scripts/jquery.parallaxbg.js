(function ($) {

	'use strict';

	// Default options
	var defaults = {
			debug								: 0,
			direction						: 'vertical',
			leftOffset					: 0,
			leftUnit						: 'px',
			topOffset						: 0,
			topUnit							: 'px',
			scrollOffsetTop			: 0,
			scrollOffsetBottom	: false,
			speed								: 1,
		};

	// Throttle function prevent browser from calling function to often on scroll event
	function throttle (callback, interval) {
		var time = Date.now();

		return function () {
			if ((time + interval - Date.now()) < 0) {
				callback();
				time = Date.now();
			}
		};
	}

	$.fn.parallaxBg = function (options) {

		var
			config = $.extend({}, defaults, options),

			_self = this,

			$window = $(window);

		function changePosition () {
			if (config.direction == 'vertical')
				_self.css('background-position', config.leftOffset + config.leftUnit  + ' ' + (($window.scrollTop() * config.speed) + config.topOffset) + config.topUnit);

			else if (config.direction == 'horizontal')
				_self.css('background-position', (($window.scrollTop() * config.speed) + config.leftOffset) + config.leftUnit + ' ' + config.topOffset + config.topUnit);
		}

		$(function () {
			if (config.debug);
				console.info('Parallax BG: Plugin loaded');

			if (config.debug && config.direction == 'vertical')


			changePosition();

			$window.on('scroll.parallaxBg', throttle(changePosition, 40));
		});
	};

})(jQuery);
