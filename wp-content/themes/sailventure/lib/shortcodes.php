<?php

// [sailventure]
function sailventure_logotype () {
	return '<svg class="m-logotype"><use xlink:href="#logo-type"></use></svg>';
}
add_shortcode('sailventure', 'sailventure_logotype');

// [gallery]
function sailventure_gallery ($atts) {
	if (empty($atts['ids']))
		return;

	$gallery_images = get_posts(
		array(
			'post_type' => 'attachment',
			'include' => $atts['ids'],
			'orderby' => 'post__in'
		)
	);

	if (sizeof($gallery_images) <= 0)
		return;

	$gallery_ID = rand();

	$output = '<div class="m-gallery [ row fluid ]">';

	foreach ($gallery_images as $image) {
		$image_thumb = wp_get_attachment_image_src($image->ID, 'gallery-thumb');
		$output .= '<a href="' . $image->guid . '" target="_blank" data-dialog="#image-' . $gallery_ID . '-' . $image->ID . '" class="m-gallery__image [ xs-12 sm-6 md-4 lg-3 xl-2 column ]" style="background-image: url(' . $image_thumb[0] . ');"></a>';
	}

	$output .= '<div class="m-gallery__dialog-overflow">';

	foreach ($gallery_images as $image) {
		$output .= '<div id="image-' . $gallery_ID . '-' . $image->ID . '" class="m-gallery__dialog">';
		$output .= '<img src="' . $image->guid . '">';
		$output .= '<button class="m-icon close" data-close="#image-' . $gallery_ID . '-' . $image->ID . '"><svg class="icon32"><use xlink:href="#icon32-close"></use></svg></button>';
		$output .= '</div>';
	}

	$output .= '</div>';
	$output .= '</div>';

	return $output;
}
add_shortcode('gallery', 'sailventure_gallery');
