<?php

if (!function_exists('get_post_image_url')) {
	function get_post_image_url ($id, $size = null) {
		$size = ($size) ? $size : 'post-thumbnail';
		$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($id), $size);
		return $thumbnail[0];
	}
}

if (!function_exists('the_post_image_url')) {
	function the_post_image_url ($id, $size = null) {
		$size = ($size) ? $size : 'post-thumbnail';
		echo future_get_post_image_url($id, $size);
	}
}

if (!function_exists('get_page_template_name')) {
	function get_page_template_name ($post = null) {
		$post = get_post($post);

		if (!$post)
			return false;

		return substr(basename(get_page_template_slug($post->ID)), 5, -4);
	}
}
