<?php

// Add custom classes to body tag
add_filter('body_class', function ($classes) {

	// 'is-mobile' for mobile user agents
	if (wp_is_mobile())
		array_push($classes, 'is-mobile');

	// 'is-search' for search results
	if (is_search())
		array_push($classes, 'is-searching');

	return $classes;
});
