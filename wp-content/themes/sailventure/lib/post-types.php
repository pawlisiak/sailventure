<?php

// Include post types files
$includes = array(
	'lib/post-types/cruise.php'				// Cruise
);

array_walk($includes, function ($file) {
	if (!locate_template($file, true, true))
		trigger_error(sprintf(__('Error locating %s for inclusion', 'sailventure'), $file), E_USER_ERROR);
});
