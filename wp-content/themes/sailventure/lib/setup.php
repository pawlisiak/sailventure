<?php

// Enqueue theme script and styles
add_action('wp_enqueue_scripts', function () {
	$theme_meta = wp_get_theme();

	wp_enqueue_style('sailventure_styles', get_template_directory_uri() . '/assets/dist/styles/styles.min.css', /*$theme_meta->version*/time(), null);
	wp_enqueue_script('sailventure_scripts', get_template_directory_uri() . '/assets/dist/scripts/scripts.min.js', ['jquery'], /*$theme_meta->version*/time(), true);
	wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDzzFbXu9YeuKnPtZp54CXMBkhnchjSGwk');
}, 100);

add_action('after_setup_theme', function () {
	// Add featured image support
	add_theme_support('post-thumbnails');

	// Add image sizes
	add_image_size('gallery-thumb', 320, 320, true);
	add_image_size('cruise-thumb', 280, 540, true);

	// Add nav menus
	register_nav_menus(array(
		'header-menu' => __('Primary header menu', 'sailventure'),
		'footer-menu' => __('Footer menu', 'sailventure')
	));
});

// Google Maps API ACF fix
function my_acf_google_map_api ($api) {
	$api['key'] = 'AIzaSyDzzFbXu9YeuKnPtZp54CXMBkhnchjSGwk';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
