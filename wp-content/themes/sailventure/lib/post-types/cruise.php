<?php

add_action('init', function () {
	$labels = array(
		'name'				=> __('Cruises', 'sailventure'),
		'singular_name'		=> __('Cruise', 'sailventure'),
		'not_found'			=> __('No cruises found', 'sailventure')
	);

	$args = array(
		'labels'			=> $labels,
		'public'			=> true,
		'menu_position'		=> 25,
		'supports'			=> array('title', 'editor', 'thumbnail', 'page-attributes')
	);

	register_post_type('cruise', $args);
});
