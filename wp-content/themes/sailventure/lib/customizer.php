<?php

class SailVenture_Customizer {
	public static function register ($wp_customize) {

		// $customize_options is array of sections and settings. You can set it up as you wish with following parameters:

		// SECTION PARAMETERS:
		// -------------------

		// * id (required) - a unique slug-like string to use as an id (notice that function below add 'sailventure_' prefix to this parameter),
		// * title (required) - the visible name of a controller section,
		// * description (optional) - this argument can add additional descriptive text to the section,
		// * priority (optional) - this controls the order in which this section appears in the Theme Customizer sidebar,
		// * settings (optional) - array of section settings (more information below).

		// SETTINGS PARAMETERS:
		// --------------------

		// * id (required) - a unique string or a specific customization controller object,
		// * title (optional) - the visible name of a controller,
		// * description (optional) - this argument can add additional descriptive text to the controller,
		// * priority (optional) - this controls the order in which this controller appears in the section,
		// * default (optional) - a default value for the setting if none is defined,
		// * type (optional) - supported types include: text, image, checkbox, radio, select, textarea, dropdown-pages, email, url, number, hidden, and date (default is 'text').
		// * choices (optional) - if 'type' parameter is 'radio' or 'select' you can put there array witch choices to choose (e.g. array('yes' => 'Yes', 'no' => 'No, thanks')).

		$customise_options = array(
			array(
				'id'						=> 'frontpage',
				'title'					=> __('Front Page', 'sailventure'),
				'settings'			=> array(
					array(
						'id'				=> 'hero_image',
						'title'			=> __('Background image of hero banner', 'sailventure'),
						'type'			=> 'image'
					),
					array(
						'id'				=> 'hero_color',
						'title'			=> __('Color of paragraph text inside hero banner', 'sailventure'),
						'type'			=> 'color',
						'default'		=> '#ffffff'
					),
					array(
						'id'				=> 'hero_accents',
						'title'			=> __('Color of accent text inside hero banner', 'sailventure'),
						'type'			=> 'color',
						'default'		=> '#8ae1dc'
					)
				)
			),
			array(
				'id'					=> 'cruises',
				'title'					=> __('Cruises settings', 'sailventure'),
				'settings'				=> array(
					array(
						'id'			=> 'cruise_form',
						'title'			=> __('Purchase cruise form', 'sailventure'),
						'description'	=> __('Paste there the form shortcode', 'sailventure'),
						'type'			=> 'text'
					)
				)
			),
			array(
				'id'			=> 'cookies_info',
				'priority'		=> 900,
				'title'			=> __('Cookies info', 'sailventure'),
				'settings'		=> array(
					array(
						'id'			=> 'cookie_info',
						'title'			=> __('Content of cookies information', 'sailventure'),
						'description'	=> '',
						'default'		=> 'This website stores cookies on your computer. More about <a href="/cookies/">Cookies</a>.',
						'type'			=> 'textarea'
					),
					array(
						'id'			=> 'cookie_button',
						'title'			=> __('Content of accept button', 'sailventure'),
						'default'		=> 'Ok, shure',
						'type'			=> 'text'
					)
				)
			)
		);

		foreach ($customise_options as $option) {

			if (isset($option['id']) && isset($option['title'])) {

				// Add section
				$wp_customize->add_section('sailventure_' . $option['id'],
					array(
						'title'			=> $option['title'],
						'priority'		=> (isset($option['priority'])) ? $option['priority'] : false,
						'capability'	=> 'edit_theme_options',
						'description'	=> (isset($option['description'])) ? $option['description'] : false
					)
				);

				if (isset($option['settings'])) {

					foreach ($option['settings'] as $setting) {

						if (isset($setting['id'])) {

							$setting_args = array(
								'type'			=> 'theme_mod',
								'capability'	=> 'edit_theme_options',
								'transport'		=> 'refresh',
								'default'		=> (isset($setting['default'])) ? $setting['default'] : false,
							);

							$control_args = array(
								'label'			=> (isset($setting['title'])) ? $setting['title'] : false,
								'description'	=> (isset($setting['description'])) ? $setting['description'] : false,
								'section'		=> 'sailventure_' . $option['id'],
								'settings'		=> $setting['id'],
								'priority'		=> (isset($setting['priority'])) ? $setting['priority'] : false,
								'type'			=> (isset($setting['type'])) ? $setting['type'] : 'text',
								'choices'		=> (in_array($setting['type'], array('select', 'radio')) && isset($setting['choices'])) ? $setting['choices'] : false
							);

							// Add setting
							$wp_customize->add_setting($setting['id'], $setting_args);

							// Add control
							switch ($control_args['type']) {
								case 'image' :
									$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, $setting['id'] . '_option', $control_args));
									break;

								case 'color' :
									$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, $setting['id'] . '_option', $control_args));
									break;

								default :
									$wp_customize->add_control(new WP_Customize_Control($wp_customize, $setting['id'] . '_option', $control_args));
									break;
							}
						}
					}
				}
			}
		}
	}
}

add_action('customize_register', array('SailVenture_Customizer', 'register'));
