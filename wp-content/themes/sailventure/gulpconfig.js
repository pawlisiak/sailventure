'use strict';

module.exports = function() {
	var
		domain = 'http://sailventure.dev/',

		zipName = 'sailventure',
		zipInclude = ['dist', 'lang', 'lib', 'templates', 'README.md', 'LICENSE', '*.php', '*.css'],

		assetsPath = './assets/src/',
		distPath = './assets/dist/',

		scriptsArray = [
			assetsPath + 'scripts/jquery.affix.js',
			assetsPath + 'scripts/jquery.parallaxbg.js',
			assetsPath + 'scripts/jquery.svgloader.js',
			assetsPath + 'scripts/google.map.js',
			assetsPath + 'scripts/custom.js'
		],

		compatibility = [
			'last 2 versions',
			'ie >= 9',
			'Android >= 2.3'
		],

		errorNotify = true;

	return {
		domain			: domain,
		zipName			: zipName,
		zipInclude		: zipInclude,
		assetsPath		: assetsPath,
		distPath		: distPath,
		scriptsArray	: scriptsArray,
		compatibility	: compatibility,
		errorNotify		: errorNotify
	};
};
